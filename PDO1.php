<?php


try {
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', ''); // tester la connexion à la bdd
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage()); // si ratée affichée la connexion à la bdd
}
// Si tout va bien, on peut continuer




$reponse = $bdd->query('SELECT * FROM jeux_video'); // On récupère tout le contenu de la table jeux_video non traitable en l'état




while ($donnees = $reponse->fetch()) // On affiche ligne par ligne chaque entrée une à une tant que $reponse->fetch = true

{
    echo "<p>".$donnees['nom']." - <strong> ".$donnees['console']."</strong></p><br/>";
}

$reponse->closeCursor(); // Termine le traitement de la requête




?>