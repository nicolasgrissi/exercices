<?php

if (preg_match("#^j'aime #","j'aime jouer de du piano")){
    echo "VRAI pour la 1, la vérification contient le mot \"j'aime\"";
}
else{
    echo "FAUX pour la 1";
}

echo "<br>";

if (preg_match("#guitare|banjo#","j'aime jouer de la guitare")){
    echo "VRAI pour la 2, la vérification contient le mot guitare OU le mot banjo";
}
else{
    echo "FAUX pour la 2";
}

echo "<br>";

if (preg_match("#^j'aime #","j'aime jouer de du piano")){
    echo "VRAI pour la 3,  la vérification commence par \"j'aime\"";
}
else{
    echo "FAUX pour la 3";
}

echo "<br>";

if (preg_match("#piano$#","j'aime jouer de du piano")){
    echo "VRAI pour la 4,  la vérification se termine bien par piano";
}
else{
    echo "FAUX pour la 4";
}

echo "<br>";

if (preg_match("#gr[iao]s#","La nuit tous les chats sont gris")){
    echo "VRAI pour la 5,  la vérification contient bien un mot gr[iao]s";
}
else{
    echo "FAUX pour la 5";
}

echo "<br>";

if (preg_match("#gr[a-z]s#","La nuit tous les chats sont gris")){
    echo "VRAI pour la 6,  la vérification contient bien un mot gr[a-z]s avec l'intervalle de a-z";
}
else{
    echo "FAUX pour la 6";
}

echo "<br>";

if (preg_match("#gr[a-zA-Z]s#","La nuit tous les chats sont grIs")){
    echo "VRAI pour la 7,  la vérification contient bien un mot gr[a-zA-z]s avec l'intervalle de a-z et A-Z avec donc les majuscules , pareil pour les chiffres [0-9]";
}
else{
    echo "FAUX pour la 7";
}

echo "<br>";

if (preg_match("#gr[^a-zA-Z]s#","La nuit tous les chats sont gr1s")){
    echo "VRAI pour la 8,  la vérification contient bien tout sauf un mot gr[^a-zA-z]s avec l'intervalle de a-z et A-Z avec donc les majuscules , pareil pour les chiffres [0-9]";
}
else{
    echo "FAUX pour la 8";
}

echo "<br>";


if (preg_match("#Yo(ay)+#","Yoayay")){
    echo "VRAI pour la 9,  la vérification contient bien Yo(ay)+ au moins une fois et plus";
}
else{
    echo "FAUX pour la 9";
}

echo "<br>";


if (preg_match("#Yo(ay)?#","Yoayay")){
    echo "VRAI pour la 10,  la vérification contient bien Yo(ay)? au moins une fois ou 0 fois";
}
else{
    echo "FAUX pour la 10";
}

echo "<br>";

if (preg_match("#Yo(ay){5}#","Yoayayayayayay")){
    echo "VRAI pour la 11,  la vérification contient bien Yo(ay){5} 5 fois au moins";
}
else{
    echo "FAUX pour la 11";
}

echo "<br>";

if (preg_match("#Yo(ay){5}$#","Yoayayayayay")){
    echo "VRAI pour la 12,  la vérification contient bien Yo(ay){5}$ 5 fois précisement";
}
else{
    echo "FAUX pour la 12";
}

echo "<br>";

if (preg_match("#^[0-9]{3}#","1987")){
    echo "VRAI pour la 13,  la vérification contient bien ^[0-9]{3} au début 3 chiffres de 0 à 9 (1987)";
}
else{
    echo "FAUX pour la 13";
}

echo "<br>";

if (preg_match("#^[0-9]{4}$#","1987")){
    echo "VRAI pour la 14,  la vérification contient bien ^[0-9]{4}$ au complets 4 chiffres de 0 à 9 (1987)";
}
else{
    echo "FAUX pour la 14";
}



echo "<br>";

echo "<br>";

$retour = preg_replace("#^([0-9]{2})/([0-9]{2})/([0-9]{4})$#", "$1-$2-$3" ,"19/08/1988");

echo "l'année formatée avec les REGEX est : ". $retour;











